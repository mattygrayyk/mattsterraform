# the main build file of the environment
# STEP 1 - Build VPC

module "eks_vpc_build" {
  source = "../vpc-modules"
  ### VPC
  vpc_region = "eu-west-1"
  vpc_name = "to-be-sure-eks-test05"
  vpc_cidr = "172.25.48.0/20"
  name = "to-be-sure-eks-test05"
  subnet1_cidr = "172.25.51.0/24"
  subnet1_name = "172.25.51.0-TEST-VPC-SUBNET-2A"
  subnet2_cidr = "172.25.52.0/24"
  subnet2_name = "172.25.52.0-TEST-VPC-SUBNET-2B"
  subnet3_cidr = "172.25.53.0/24"
  subnet3_name = "172.25.53.0-TEST-VPC-SUBNET-2C"
  ### EKS Cluster
  master_sg_name = "tbs-eks-master-nsg"
  worker_sg_name = "tbs-eks-worker-nsg"
  inst_key_name = "tbs-key"
  eks_region = "eu-west-1"
  cluster_name = "to-be-sure-eks-cluster"
  coop_eks_autoscaling_group = "to-be-sure-autoscaling-group"
  ###### Public (Ingress from the internet) Networking
  coop-pub-subnet-az = "eu-west-1a"
  public_sg_name = "tbs-eks-public-nsg"
  public_igw_name = "tbs-public-igw"
  public_igw_route_table = "tbs-igw-route-table"
  subnet_public_cidr = "172.25.50.0/24"
  subnet_public_name = "tbs-public-access-subnet"
  ###### EKS Nat gateway for docker image retrieval
  eks_nat_gateway_name = "tbs-nat-gateway"
  eks_nat_gateway_route_table_name = "tbs-nat-gateway-route-table"

}
