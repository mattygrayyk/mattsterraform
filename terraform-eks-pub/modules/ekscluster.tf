locals {
   coop_eks_sg_id = "${aws_security_group.coop-eks-master-nsg.id}"
}


resource "aws_eks_cluster" "coop-eks-cluster" {
  name     = "${var.cluster_name}"
  role_arn = "${aws_iam_role.coop_eks_cluster_role.arn}"

  vpc_config {
    subnet_ids              = ["${aws_subnet.coop-vpc-subnet-2a.id}","${aws_subnet.coop-vpc-subnet-2b.id}","${aws_subnet.coop-vpc-subnet-2c.id}"]
    security_group_ids      = ["${local.coop_eks_sg_id}"]
    endpoint_private_access = "true"
    endpoint_public_access  = "true"

  }

  depends_on = [
    "aws_iam_role_policy_attachment.cluster_AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.cluster_AmazonEKSServicePolicy",
  ]
}
output "endpoint" {
  value = "${aws_eks_cluster.coop-eks-cluster.endpoint}"
}

output "kubeconfig-certificate-authority-data" {
  value = "${aws_eks_cluster.coop-eks-cluster.certificate_authority.0.data}"
}
