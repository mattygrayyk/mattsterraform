

# this defines the image type , to be changed later
data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu-eks/k8s_1.10/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20190212"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

data "aws_ami" "eks-worker" {
  filter {
    name   = "name"
    values = ["amazon-eks-node-${aws_eks_cluster.coop-eks-cluster.version}-v*"]
  }

  most_recent = true
  owners      = ["602401143452"] # Amazon EKS AMI Account ID

}



#This is userdata for AWS AMI
locals {
  worker-userdata = <<USERDATA
#!/bin/bash
set -o xtrace
/etc/eks/bootstrap.sh --apiserver-endpoint '${aws_eks_cluster.coop-eks-cluster.endpoint}' --b64-cluster-ca '${aws_eks_cluster.coop-eks-cluster.certificate_authority.0.data}' '${var.cluster_name}'
USERDATA
}



resource "aws_launch_configuration" "coop-aws-launch-configuration" {
  associate_public_ip_address = "true"
  name_prefix   = "coop-aws-launch-configuration"
  iam_instance_profile = "coop-eks-inst-prof"
  image_id      = "${data.aws_ami.eks-worker.id}"
  instance_type = "t2.medium"
  security_groups = ["${aws_security_group.coop-eks-worker-nsg.id}"]
  key_name      = "${var.inst_key_name}"
  user_data_base64            = "${base64encode(local.worker-userdata)}"
  lifecycle {
    create_before_destroy = true
  }
}




resource "aws_autoscaling_group" "coop-eks-autoscaling-group" {
  name                      = "${var.coop_eks_autoscaling_group}"
  max_size                  = 5
  min_size                  = 2
  desired_capacity          = 3
#  placement_group           = "coop-eks-autoscaling-group"
  launch_configuration      = "${aws_launch_configuration.coop-aws-launch-configuration.id}"
  vpc_zone_identifier       = ["${aws_subnet.coop-vpc-subnet-2a.id}", "${aws_subnet.coop-vpc-subnet-2b.id}", "${aws_subnet.coop-vpc-subnet-2c.id}"]

  tag {
    key                 = "env"
    value               = "coop-eks-worker"
    propagate_at_launch = true
  }
  tag {
    key                 = "kubernetes.io/cluster/${var.cluster_name}"
    value               = "owned"
    propagate_at_launch = true
  }
}
