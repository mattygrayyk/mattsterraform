resource "aws_subnet" "coop-vpc-subnet-2a" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet1_cidr}"
  availability_zone = "eu-west-2a"
  tags = {
    Name = "${var.subnet1_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_subnet" "coop-vpc-subnet-2b" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet2_cidr}"
  availability_zone = "eu-west-2b"
  tags = {
    Name = "${var.subnet2_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_subnet" "coop-vpc-subnet-2c" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet3_cidr}"
  availability_zone = "eu-west-2c"
  tags = {
    Name = "${var.subnet3_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}
