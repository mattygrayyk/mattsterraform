resource "aws_vpc" "coop_vpc" {
  cidr_block       = "${var.cidr}"
  instance_tenancy = "${var.instance_tenancy}"
  enable_dns_support      = "true"
  enable_dns_hostnames    = "true"
  tags = {
    Name = "${var.vpc_name}"
    Terraform                                   = "true"
    Environment                                 = "${var.environment_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}


#########
# this bit creates the internetgateway etc

resource "aws_internet_gateway" "coop-eks-internet-gw" {
  vpc_id = "${aws_vpc.coop_vpc.id}"

  tags = {
    Name = "${var.coop-eks-internet-gw}"
  }
}

resource "aws_route_table" "coop-eks-route" {
  vpc_id = "${aws_vpc.coop_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.coop-eks-internet-gw.id}"
  }
}

resource "aws_route_table_association" "coop-eks-route-ass1" {

  subnet_id      = "${aws_subnet.coop-vpc-subnet-2a.id}"
  route_table_id = "${aws_route_table.coop-eks-route.id}"
}

resource "aws_route_table_association" "coop-eks-route-ass2" {

  subnet_id      = "${aws_subnet.coop-vpc-subnet-2b.id}"
  route_table_id = "${aws_route_table.coop-eks-route.id}"
}
resource "aws_route_table_association" "coop-eks-route-ass3" {

  subnet_id      = "${aws_subnet.coop-vpc-subnet-2c.id}"
  route_table_id = "${aws_route_table.coop-eks-route.id}"
}
