# Master Security group

resource "aws_security_group" "coop-eks-master-nsg" {
  name        = "${var.master_sg_name}"
  description = "${var.master_sg_name}"
  vpc_id      = "${aws_vpc.coop_vpc.id}"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
  Name = "${var.master_sg_name}"
}
}

resource "aws_security_group_rule" "master-cluster-worker-ingress" {
  description              = "Allow the workers to communicate with the API servers on port 443"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.coop-eks-master-nsg.id}"
  source_security_group_id = "${aws_security_group.coop-eks-worker-nsg.id}"
  to_port                  = 443
  type                     = "ingress"
}

########  Worker Nodes Security Group
resource "aws_security_group" "coop-eks-worker-nsg" {
  name        = "${var.worker_sg_name}"
  description = "${var.worker_sg_name}"
  vpc_id      = "${aws_vpc.coop_vpc.id}"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = "${
    map(
     "Name", "${var.worker_sg_name}",
     "kubernetes.io/cluster/${var.cluster_name}", "owned",
    )
  }"
}

resource "aws_security_group_rule" "work-node-ingress-self" {
  description              = "Allow node to communicate with each other"
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = "${aws_security_group.coop-eks-worker-nsg.id}"
  source_security_group_id = "${aws_security_group.coop-eks-worker-nsg.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "work-node-ingress-cluster" {
  description              = "Allow worker Kubelets and pods to receive communication from the cluster control plane"
  from_port                = 1025
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.coop-eks-worker-nsg.id}"
  source_security_group_id = "${aws_security_group.coop-eks-worker-nsg.id}"
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "work-control-plane-egress-cluster" {
  description              = "Allow pods to communicate with the culster API Server"
  from_port                = 443
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.coop-eks-worker-nsg.id}"
  source_security_group_id = "${aws_security_group.coop-eks-worker-nsg.id}"
  to_port                  = 443
  type                     = "ingress"
}
