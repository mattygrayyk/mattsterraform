# the main build file of the environment
# STEP 1 - Build VPC

module "kube-vpc" {
  vpc_name = "coop_vpc_test2"
  source = "../modules"
  cidr = "10.158.0.0/16"
  name = "coop-eks-vpc-test1"
  subnet1_cidr = "10.158.11.0/24"
  subnet1_name = "10-158-11-0-TEST-VPC-SUBNET-2A"
  subnet2_cidr = "10.158.12.0/24"
  subnet2_name = "10-158-12-0-TEST-VPC-SUBNET-2B"
  subnet3_cidr = "10.158.13.0/24"
  subnet3_name = "10-158-13-0-TEST-VPC-SUBNET-2C"
  master_sg_name = "coop-eks-master-nsg-test2"
  worker_sg_name = "coop-eks-worker-nsg-test2"
  public_sg_name = "coop-eks-public-nsg-test2"
  # EKS Cluster
  eks_region = "eu-west-2"
  cluster_name = "coop-eks-cluster-test2"
  coop-eks-internet-gw = "eks-test2-igw"
  coop_eks_autoscaling_group = "eks-asg-test2"
  inst_key_name = "mattskp"
}
