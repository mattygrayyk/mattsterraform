resource "aws_vpc" "coop_vpc" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "${var.instance_tenancy}"
  enable_dns_support      = "true"
  enable_dns_hostnames    = "true"
  tags = {
    Name = "${var.vpc_name}"
    Terraform                                   = "true"
    Environment                                 = "${var.environment_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }
}
