locals {
  keysa =  [
   "Name",
   "kubernetes.io/cluster/${var.cluster_name}",
   "kubernetes.io/role/internal-elb",
   ]
  valuesa = [
   "${var.subnet1_name}",
   "shared",
   "1"
  ]
}

locals {
  keysb =  [
   "Name",
   "kubernetes.io/cluster/${var.cluster_name}",
   "kubernetes.io/role/internal-elb",
   ]
  valuesb = [
   "${var.subnet2_name}",
   "shared",
   "1"
  ]
}


locals {
  keysc =  [
   "Name",
   "kubernetes.io/cluster/${var.cluster_name}",
   "kubernetes.io/role/internal-elb",
   ]
  valuesc = [
   "${var.subnet3_name}",
   "shared",
   "1"
  ]
}

resource "aws_subnet" "coop-vpc-subnet-2a" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet1_cidr}"
  availability_zone = "${var.vpc_region}a"
  tags = "${zipmap(local.keysa, local.valuesa)}"
}

resource "aws_subnet" "coop-vpc-subnet-2b" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet2_cidr}"
  availability_zone = "${var.vpc_region}b"
  tags = "${zipmap(local.keysb, local.valuesb)}"
}

resource "aws_subnet" "coop-vpc-subnet-2c" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet3_cidr}"
  availability_zone = "${var.vpc_region}c"
  tags = "${zipmap(local.keysc, local.valuesc)}"
}


resource "aws_subnet" "coop-pub-subnet" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet_public_cidr}"
  availability_zone = "${var.coop-pub-subnet-az}"
  tags = {
    Name = "${var.subnet_public_name}"
  }
}
