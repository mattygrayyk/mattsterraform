provider "aws" {
assume_role {
  role_arn = "arn:aws:iam::100168932885:role/TerraformChildRole"
 }
 region = "eu-west-1"
}
