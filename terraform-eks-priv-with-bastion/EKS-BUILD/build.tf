# the main build file of the environment
# STEP 1 - Build VPC

module "eks_vpc_build" {
  source = "../vpc-modules"
  ### VPC
  vpc_region = "eu-west-1"
  vpc_name = "small-change-eks-test06"
  cidr = "172.26.48.0/20"
  name = "small-change-eks-test06"
  subnet1_cidr = "172.26.51.0/24"
  subnet1_name = "172.26.51.0-TEST-VPC-SUBNET-2A"
  subnet2_cidr = "172.26.52.0/24"
  subnet2_name = "172.26.52.0-TEST-VPC-SUBNET-2B"
  subnet3_cidr = "172.26.53.0/24"
  subnet3_name = "172.26.53.0-TEST-VPC-SUBNET-2C"
  ### EKS Cluster
  master_sg_name = "sc-test06-eks-master-nsg"
  worker_sg_name = "sc-test06-eks-worker-nsg"
  inst_key_name = "small-change"
  eks_region = "eu-west-1"
  cluster_name = "sc-test06-eks-cluster"
  coop_eks_autoscaling_group = "sc-test06-autoscaling-group"
  ###### Public (Ingress from the internet) Networking
  public_sg_name = "sc-test06-eks-public-nsg"
  public_igw_name = "sc-test06-public-igw"
  public_igw_route_table = "sc-test06-igw-route-table"
  subnet_public_cidr = "172.26.50.0/24"
  subnet_public_name = "sc-test06-public-access-subnet"
  ###### EKS Nat gateway for docker image retrieval
  eks_nat_gateway_name = "sc-test06-nat-gateway"
  eks_nat_gateway_route_table_name = "sc-test06-nat-gateway-route-table"
  ###### Bastion host
  bastion_host_az = "eu-west-1a"
  bastion_host_name = "test06-bastion-host"
  ami_image_owner = "801119661308"
}
