#NOTE - This is specific for the small chnage environment

terraform {
  backend "s3" {
    encrypt=true
    bucket="terraformbackendstate"
    region="eu-west-2"
    key="yorkshireterraform"
  }
}
