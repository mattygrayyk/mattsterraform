#
# PUBLIC
#

resource "aws_internet_gateway" "coop-vpc-igw" {
  vpc_id = "${aws_vpc.coop_vpc.id}"

  tags = {
    Name = "${var.public_igw_name}"
  }
}

resource "aws_route_table" "eks-internet-gateway-route-table" {
  vpc_id = "${aws_vpc.coop_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.coop-vpc-igw.id}"
  }
  tags = {
    Name = "${var.public_igw_route_table}"
  }
}

resource "aws_route_table_association" "eks-public-access-route-subnet-association" {
  subnet_id      = "${aws_subnet.coop-pub-subnet.id}"
  route_table_id = "${aws_route_table.eks-internet-gateway-route-table.id}"
}


############   NAT GATEWAY

resource "aws_eip" "coop-eks-natgw-eip" {
      vpc = true
}



resource "aws_nat_gateway" "eks-nat-gateway" {
      allocation_id = "${aws_eip.coop-eks-natgw-eip.id}"
      subnet_id = "${aws_subnet.coop-pub-subnet.id}"
      tags = {
        Name = "${var.eks_nat_gateway_name}"
      }

}

resource "aws_route_table" "eks-nat-gateway-route-table" {
  vpc_id = "${aws_vpc.coop_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.eks-nat-gateway.id}"
  }
  tags = {
    Name = "${var.eks_nat_gateway_route_table_name}"
  }
}

resource "aws_route_table_association" "eks-nat-gateway-route-subnet-association1" {
  subnet_id      = "${aws_subnet.coop-vpc-subnet-2a.id}"
  route_table_id = "${aws_route_table.eks-nat-gateway-route-table.id}"
}

resource "aws_route_table_association" "eks-nat-gateway-route-subnet-association2" {
  subnet_id      = "${aws_subnet.coop-vpc-subnet-2b.id}"
  route_table_id = "${aws_route_table.eks-nat-gateway-route-table.id}"
}

resource "aws_route_table_association" "eks-nat-gateway-route-subnet-association3" {
  subnet_id      = "${aws_subnet.coop-vpc-subnet-2c.id}"
  route_table_id = "${aws_route_table.eks-nat-gateway-route-table.id}"
}
