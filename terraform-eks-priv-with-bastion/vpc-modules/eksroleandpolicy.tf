# Define the role the
resource "aws_iam_role" "coop_eks_cluster_role" {
  name           = "${var.cluster_name}-eks-role"
  assume_role_policy = <<POLICY
{
"Version": "2012-10-17",
"Statement": [
  {
    "Effect": "Allow",
    "Principal": {
      "Service": "eks.amazonaws.com"
    },
    "Action": "sts:AssumeRole"
  }
]
}
POLICY

#  assume_role_policy    = "${data.aws_iam_policy_document.cluster_assume_role_policy.json}"
#  permissions_boundary  = "${var.permissions_boundary}"
#  path                  = "${var.iam_path}"
#  force_detach_policies = true
}

resource "aws_iam_role" "coop-ec2-role" {
  name = "${var.cluster_name}-ec2-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}


resource "aws_iam_instance_profile" "coop_instance_prof" {
  name = "coop-eks-node-prof"
  role = "${aws_iam_role.coop_eks_cluster_role.name}"
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = "${aws_iam_role.coop_eks_cluster_role.name}"
}

resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = "${aws_iam_role.coop_eks_cluster_role.name}"
}
resource "aws_iam_role_policy_attachment" "coop-node-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = "${aws_iam_role.coop-ec2-role.name}"
}

resource "aws_iam_role_policy_attachment" "coop-node-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = "${aws_iam_role.coop-ec2-role.name}"
}

resource "aws_iam_role_policy_attachment" "coop-node-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = "${aws_iam_role.coop-ec2-role.name}"
}

#resource "aws_iam_role_policy_attachment" "coop-node-lb" {
#  policy_arn = "arn:aws:iam::12345678901:policy/eks-lb-policy"
#  role       = "${aws_iam_role.coop-ec2-role.name}"
#}


locals {
  config_map_aws_auth = <<CONFIGMAPAWSAUTH


apiVersion: v1
kind: ConfigMap
metadata:
  name: aws-auth
  namespace: kube-system
data:
  mapRoles: |
    - rolearn: ${aws_iam_role.coop-ec2-role.arn}
      username: system:node:{{EC2PrivateDNSName}}
      groups:
        - system:bootstrappers
        - system:nodes
CONFIGMAPAWSAUTH
}

output "config_map_aws_auth" {
  value = "${local.config_map_aws_auth}"
}
