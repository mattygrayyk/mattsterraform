
data "aws_ami" "bastion" {
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  most_recent = true
  owners      = ["${var.ami_image_owner}"] # Amazon EKS AMI Account ID

}

locals {
  bastion_sg_id = "${aws_security_group.coop-eks-public-nsg.id}"
}

locals {
  bastion_sub_id = "${aws_subnet.coop-pub-subnet.id}"
}

resource "aws_instance" "bastion_host" {
    ami = "${data.aws_ami.bastion.id}"
    instance_type = "t2.micro"
    key_name = "${var.inst_key_name}"
    vpc_security_group_ids = ["${local.bastion_sg_id}"]
    subnet_id = "${local.bastion_sub_id}"
    availability_zone = "${var.bastion_host_az}"

    tags = {
      Name = "${var.bastion_host_name}"
    }
}

resource "aws_eip" "bastion_public_eip" {
      vpc = true
}

resource "aws_eip_association" "bastion_eip_assoc" {
  instance_id   = "${aws_instance.bastion_host.id}"
  allocation_id = "${aws_eip.bastion_public_eip.id}"
}
