resource "aws_subnet" "coop-vpc-subnet-2a" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet1_cidr}"
  availability_zone = "${var.vpc_region}a"
  tags = {
    Name = "${var.subnet1_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_subnet" "coop-vpc-subnet-2b" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet2_cidr}"
  availability_zone = "${var.vpc_region}b"
  tags = {
    Name = "${var.subnet2_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}

resource "aws_subnet" "coop-vpc-subnet-2c" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet3_cidr}"
  availability_zone = "${var.vpc_region}c"
  tags = {
    Name = "${var.subnet3_name}"
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb" = "1"
  }
}
######   Bastion host subnet

resource "aws_subnet" "coop-pub-subnet" {
  vpc_id     = "${aws_vpc.coop_vpc.id}"
  cidr_block = "${var.subnet_public_cidr}"
  availability_zone = "${var.bastion_host_az}"
  tags = {
    Name = "${var.subnet_public_name}"
  }
}
