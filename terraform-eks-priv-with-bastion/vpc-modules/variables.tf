# Variables that can be used with anny resources eg for tags

variable "name" {
  description = "Name to be used on all the resources as identifier"
  default     = ""
}

# VPC variables
variable "vpc_region" {
  description = "Name used for region specific artifacts i.e subnets"
  default     = ""
}

variable "vpc_name" {
  description = "Name to be used on all the resources as identifier"
  default     = ""
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  default     = "default"
}
variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/16"
}

variable "subnet1_cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/16"
}

variable "subnet2_cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/16"
}

variable "subnet3_cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  default     = "0.0.0.0/16"
}

variable "subnet1_name" {
  description = "Name to be used on subnet 1 identifier"
  default     = ""
}

variable "subnet2_name" {
  description = "Name to be used on subnet 2 identifier"
  default     = ""
}

variable "subnet3_name" {
  description = "Name to be used on subnet 3 identifier"
  default     = ""
}

variable "subnet_public_cidr" {
  description = "The CIDR block for the public facing security group"
  default     = "0.0.0.0/16"
}

variable "subnet_public_name" {
  description = "Name to be used on public subnet identifier"
  default     = ""
}

variable "master_sg_name" {
  description = "Name to be used on all the resources as identifier"
  default     = ""
}

variable "worker_sg_name" {
  description = "Name to be used on all the resources as identifier"
  default     = ""
}

variable "public_sg_name" {
  description = "Name to be used on all the resources as identifier"
  default     = ""
}

variable "coop-eks-internet-gw" {
  description = "Internet gateway for eks access for build"
  default = ""
}


variable "inst_key_name" {
  description = "Name of key pair to use for ec2 instances"
  default     = ""
}

# EKS cluster variables
variable "environment_name" {
  description = "Name of Kubernetes Environment"
  default     = "coop-test"
}


variable "iam_path" {
  description = "If provided, all IAM roles will be created on this path."
  default     = "/"
}
variable "eks_region" {
  description = "Name of eks region"
  default     = ""
}
variable "cluster_name" {
  description = "Name of eks cluster"
  default     = ""
}

# Worker nodes

variable "coop_eks_autoscaling_group" {
  description = "Name of eks autoscaling group prefix"
  default     = ""
}

#### internet  gateway
variable "public_igw_name" {
  description = "Name of eks internet gateway"
  default     = ""
}
variable "public_igw_route_table" {
  description = "Name of eks internet gateway route table"
  default     = ""
}

#### Nat gateway
variable "eks_nat_gateway_name" {
  description = "Name of eks nat gateway"
  default     = ""
}
variable "eks_nat_gateway_route_table_name" {
  description = "Name of eks nat gateway route table"
  default     = ""
}

######  Bastion host
variable "bastion_host_name" {
  description = "Name of the bastion host tag"
  default     = ""
}
variable "bastion_host_az" {
  description = "Availabilty zone of bastion host . Must match up to the subnet"
  default     = ""
}
variable "ami_image_owner" {
  description = "Name of image owner depends on region"
  default     = ""
}
